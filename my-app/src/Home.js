import React, {Component} from 'react';
import CheckboxOrRadioGroup from './CheckboxOrRadioGroup';
import SingleInput from './SingleInput';
import TextArea from './TextArea';
import Select from './Select';

class Home extends Component {

    constructor(props) {
        super(props);
        this.defaultProps = {
            propArray: [1,2,3,4,5],
            propBool: true,

            propNumber: 1,
            propString: "String value...",

            propObject: {
                objectName1:"objectValue1",
                objectName2: "objectValue2",
                objectName3: "objectValue3"
            }
        };

        this.state = {
            ownerName: '',
            Address: '',
            Mobile: '',
            Email: '',
            Company:'',
            AllData:[],
            rows:[]
        };

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleClearForm = this.handleClearForm.bind(this);
        this.updateState=this.updateState.bind(this);
        this.propTypes = {
            propArray: React.PropTypes.array.isRequired,
            propBool: React.PropTypes.bool.isRequired,

            propNumber: React.PropTypes.number,
            propString: React.PropTypes.string,
            propObject: React.PropTypes.object
        };


    }





    componentDidMount() {

    }
    updateState(e) {
        switch (e.target.id)
        {
            case 'myInput1':this.setState({ownerName: e.target.value});break;
            case 'myInput2':this.setState({Address: e.target.value});break;
            case 'myInput3':this.setState({Mobile: e.target.value});break;
            case 'myInput4':this.setState({Email: e.target.value});break;
            case 'myInput5':this.setState({Company: e.target.value});break;

        }
    }

    handleFormSubmit(data) {
        var mydata=[];
        if(this.state.AllData)
        {
            mydata=this.state.AllData;
        }

        var data={ ownerName: this.state.ownerName,
            Address: this.state.Address,
            Mobile: this.state.Mobile,
            Email: this.state.Email,
            Company:this.state.Company};
        mydata.push(data);
        this.setState({AllData: mydata});
        var rows = [];
        this.state.AllData.forEach(function(item){
            rows.push(<tr>
                <td>{item.ownerName}</td>
                <td>{item.Address}</td>
                <td>{item.Mobile}</td>
                <td>{item.Email}</td>
                <td>{item.Company}</td>
            </tr>);
            });
        this.setState({rows:rows});

        this.setState({ ownerName: '',
            Address: '',
            Mobile: '',
            Email: '',
            Company:''});

    }

    handleClearForm() {
        this.setState({ ownerName: '',
            Address: '',
            Mobile: '',
            Email: '',
            Company:''});
    }

    render() {
        return (
            <form className="container">
                <h5>Pet Adoption Form</h5>

                <h3>Array: {this.props.propArray}</h3>
                <h3>Bool: {this.props.propBool ? "True..." : "False..."}</h3>
                <h3>Number: {this.props.propNumber}</h3>
                <h3>String: {this.props.propString}</h3>
{/*
                <h3>Object: {this.props.propObject.objectName1}</h3>
                <h3>Object: {this.props.propObject.objectName2}</h3>
                <h3>Object: {this.props.propObject.objectName3}</h3>
*/}


                <span>Name</span>
                <input type="text" value= {this.state.ownerName} onChange={this.updateState}
                      ref="myInput1" id = "myInput1"></input> <br/>
                <span>Address</span>

                <input type="text" value = {this.state.Address} onChange={this.updateState}
                       id = "myInput2"></input> <br/>
                <span>Mobile</span>

                <input type="text" value = {this.state.Mobile} onChange={this.updateState}
                       id = "myInput3"></input> <br/>
                <span>Email</span>

                <input type="text" value = {this.state.Email} onChange={this.updateState}
                       id = "myInput4"></input> <br/>
                <span>Company</span>

                <input type="text" value = {this.state.Company} onChange={this.updateState}
                       id = "myInput5"></input> <br/>

                {this.state.ownerName}<br/>
                {this.state.Address}<br/>
                {this.state.Mobile}<br/>
                {this.state.Email}<br/>
                {this.state.Company}<br/>

                                <input
                    type="button" onClick={this.handleFormSubmit}
                    className="btn btn-primary float-right"
                    value="Submit"/>
                <button type="button"
                    className="btn btn-link float-left"
                    onClick={this.handleClearForm}>Clear form
                </button>

                           <table >
                <thead>
                <tr>
                    <th> Name</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Company</th>
                </tr>
                </thead>
                <tbody>
                {this.state.rows}
                </tbody>
            </table>      </form>
        );
    }
};
export default  Home;